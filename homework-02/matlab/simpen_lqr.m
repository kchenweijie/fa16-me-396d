%% Clear all variables
clear all;

%% Define constants
% Gravitational constant
g = 9.8;

%% Define system parameters
% Pendulum mass
m = 1.0;
% Pendulum length
l = 0.5;
% Friction coefficient
b = 0.1;

%% Define goal points
% Goal state
xG = [deg2rad(180.0);
      0.0           ];
% Goal input
uG = 0.0;

%% Compute state matrices
% State equation A matrix
A = [0.0            , 1.0       ;
     -g/l*cos(xG(1)), -b/(m*l^2)];
% State equation B matrix
B = [0.0        ;
     1.0/(m*l^2)];

%% Define cost function parameters
% State cost matrix
Q = diag([10.0, 1.0]);
% Control cost matrix
R = 15.0;
% Cross cost matrix
N = [0.0;
     0.0];

%% Compute LQR parameters
[K,S,E] = lqr(A, B, Q, R, N);

%% Setup simulation
% Define time horizon
T = 60.0;
% Define time step
dt = 0.01;

% Create and initialize state vector
x = [deg2rad(0.0);
     0.0         ];
u = 0.0;
% Create and initialize error vectors
xbar = x-xG;
ubar = u-uG;

% Create and initialize results matrices
xs = x;
us = u;
xbars = xbar;
ubars = ubar;

%% Run simulation
for i=dt:dt:T
    % Compute ubar
    ubar = -K*xbar;
    
    % Compute xdot
    xbardot = A*xbar + B*ubar;
    
    % Update x
    xbar = xbar + xbardot*dt;
    
    % Compute x and u
    x = xG + xbar;
    u = uG + ubar;
    
    % Append results to results matrices
    xs = [xs x];
    us = [us u];
    xbars = [xbars xbar];
    ubars = [ubars ubar];
end

%% Plot results
figure(1);
subplot(4, 1, 1);
plot(0:dt:T, rad2deg(xs(1, :)));
subplot(4, 1, 2);
plot(0:dt:T, rad2deg(xbars(1, :)));
subplot(4, 1, 3);
plot(0:dt:T, us);
subplot(4, 1, 4);
plot(0:dt:T, ubars);