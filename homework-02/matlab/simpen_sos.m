%% Clear all variables
clear all;

%% Define constants
% Gravitational constant
g = 9.8;

%% Define system parameters
% Pendulum mass
m = 1.0;
% Pendulum length
l = 0.5;
% Friction coefficient
b = 0.1;

%% Define goal points
% Goal state
xG = [deg2rad(180.0);
      0.0           ];

%% Compute state matrices
% State equation A matrix
A = [0.0            , 1.0       ;
     -g/l*cos(xG(1)), -b/(m*l^2)];
% State equation B matrix
B = [0.0        ;
     1.0/(m*l^2)];

%% Define cost function parameters
% State cost matrix
Q = diag([10.0, 1.0]);
% Control cost matrix
R = 15.0;
% Cross cost matrix
N = [0.0;
     0.0];

%% Compute LQR parameters
[K,S,E] = lqr(A, B, Q, R, N);

%% Create closed-loop polynomial approximation
f2x1 = -g/l*cos(xG(1))-K(1)/(m*l^2);
f2x2 = -b/(m*l^2)-K(2)/(m*l^2);
f2x1_2 = g/l*sin(xG(1));
f2x1_3 = g/l*cos(xG(1));
fcl = @(xbar) [xbar(2)               ;
               xbar(1)*f2x1+xbar(2)*f2x2 + ...
                   xbar(1)^2*f2x1_2/2 + ...
                   xbar(1)^3*f2x1_3/6];

%% Initialize SOS things
% Create symbolic xbar elements
syms xbar1 xbar2;
vartable = [xbar1, xbar2];

% Initialize error variable
err = 1.0e-3;

%% Run loop
for rho = 0:0.5:20.0
    % Initialize solver
    prog = sosprogram(vartable);
    
    % Create cost function
    J = [xbar1, xbar2]*S*[xbar1; xbar2];
    
    % Create Jdot
    Jdot = 2*[xbar1, xbar2]*S*fcl([xbar1; xbar2]);
    
    % Create h and add to program
    [prog, h] = sossosvar(prog, [xbar1; xbar2]);
    
    % Add inequality constraint on cost function
    prog = sosineq(prog, h*(J-rho)-Jdot-err*(xbar1^2+xbar2^2));
    
    % Add positivity constraint to h
    prog = sosineq(prog, h);
    
    % Solve SOS
    prog = sossolve(prog);
end