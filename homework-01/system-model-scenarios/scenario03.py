import numpy as np
import matplotlib.pyplot as plt

# Define time constants
cTau1 = 1.00; cTau2 = 1.00; cTau3 = 1.00
cTau4 = 2.00; cTau5 = 1.00; cTau6 = 3.00

# Define inflow resistances
cGamma11 = 3.00; cGamma22 = 1.00; cGamma32 = 2.00
cGamma33 = 1.00; cGamma35 = 1.00; cGamma36 = 1.00
cGamma57 = 2.00; cGamma64 = 15.0; cGamma68 = 15.0

# Define outflow resistances
cBeta21 = 0.30; cBeta31 = 0.50; cBeta42 = 0.30
cBeta43 = 0.80; cBeta54 = 0.30; cBeta34 = 0.20
cBeta25 = 0.30; cBeta14 = 0.23; cBeta46 = 0.44
cBeta45 = 0.10

# Define system matrices
MA = np.matrix([[-1.00/cTau1, 0.00, 0.00, cBeta14/cTau1, 0.00, 0.00], \
    [cBeta21/cTau2, -1.00/cTau2, 0.00, 0.00, cBeta25/cTau2, 0.00], \
    [cBeta31/cTau3, 0.00, -1.00/cTau3, cBeta34/cTau3, 0.00, 0.00], \
    [0.00, cBeta42/cTau4, cBeta43/cTau4, -1.00/cTau4, cBeta45/cTau4, cBeta46/cTau4], \
    [0.00, 0.00, 0.00, cBeta54/cTau5, -1.00/cTau5, 0.00], \
    [0.00, 0.00, 0.00, 0.00, 0.00, -1.00/cTau6]])
MB = np.matrix([[cGamma11/cTau1, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00], \
    [0.00, cGamma22/cTau2, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00], \
    [0.00, cGamma32/cTau3, cGamma33/cTau3, 0.00, -cGamma35/cTau3, cGamma36/cTau3, 0.00, 0.00], \
    [0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00], \
    [0.00, 0.00, 0.00, 0.00, 0.00, 0.00, cGamma57/cTau5, 0.00], \
    [0.00, 0.00, 0.00, cGamma64/cTau6, 0.00, 0.00, 0.00, cGamma68/cTau6]])

# Define input vector
vXi = np.matrix([5.00, 10.0, 10.0, 0.00, 2.00, 0.00, 0.00, 0.00]).T

# Initialize simulation
vEta = -1.0*np.linalg.inv(MA)*MB*vXi                                        # State vector
MRes = vEta                                                                 # Results matrix
dt = 1.0                                                                    # Time step

# Run simulation
for i in np.arange(0, 20, dt):
    # Change xi8 to match input scheme
    if (i>=1 and i<14):
        vXi[7] = 7.00
    else:
        vXi[7] = 0.00

    # Compute change in state variables
    vEtaDot = MA*vEta + MB*vXi
    vEtaDot = vEtaDot*dt

    # Update state vector within bounds
    vEta = np.clip(vEta + vEtaDot, 0, 100)

    # Store results
    MRes = np.append(MRes, vEta, axis=1)

# Plot outcome expectancy
plt.figure(1)
plt.plot(np.matrix([np.arange(0, 20+dt, dt)]).T, MRes[1, :].T)
plt.axis([0, 20, 0, 100])
plt.title('Outcome Expectancy ($\eta_2$)')
plt.xlabel('Time [days]')
plt.ylabel('$\eta_2$')

# Plot self-efficacy
plt.figure(2)
plt.plot(np.matrix([np.arange(0, 20+dt, dt)]).T, MRes[2, :].T)
plt.axis([0, 20, 0, 100])
plt.title('Self-efficacy ($\eta_3$)')
plt.xlabel('Time [days]')
plt.ylabel('$\eta_3$')

# Plot behavior
plt.figure(3)
plt.plot(np.matrix([np.arange(0, 20+dt, dt)]).T, MRes[3, :].T)
plt.axis([0, 20, 0, 100])
plt.title('Behavior ($\eta_4$)')
plt.xlabel('Time [days]')
plt.ylabel('$\eta_4$')

# Plot cue to action
plt.figure(4)
plt.plot(np.matrix([np.arange(0, 20+dt, dt)]).T, MRes[5, :].T)
plt.axis([0, 20, 0, 100])
plt.title('Cue to Action ($\eta_6$)')
plt.xlabel('Time [days]')
plt.ylabel('$\eta_6$')
plt.show()
