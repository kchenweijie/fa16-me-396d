import numpy as np
import cvxpy as cvx
import matplotlib.pyplot as plt

# Define time constants
cTau2 = 10.0; cTau3 = 30.0; cTau4 = 0.80
cTau5 = 2.00; cTau6 = 0.50

# Define inflow constants
cGamma311 = 0.40; cGamma29 = 2.50; cGamma57 = 1.00
cGamma510 = 0.60; cGamma64 = 1.50

# Define outflow constants
cBeta25 = 0.50; cBeta34 = 0.20; cBeta42 = 0.30
cBeta43 = 0.90; cBeta45 = 0.50; cBeta46 = 0.90
cBeta54 = 0.60

# Define other parameters
T = 60

# Define state matrices
MA = np.matrix([[-1.0/cTau2, 0, 0, cBeta25/cTau2, 0],
                [0, -1.0/cTau3, (cBeta34+cGamma311)/cTau3, 0, 0],
                [cBeta42/cTau4, cBeta43/cTau4, -1.0/cTau4, cBeta45/cTau4, cBeta46/cTau4],
                [0, 0, cBeta54/cTau5, -1.0/cTau5, 0],
                [0, 0, -cGamma64/cTau6, 0, -1.0/cTau6]])
MB = np.matrix([[0, cGamma29/cTau2, 0],
                [-cGamma311/cTau3, 0, 0],
                [0, 0, 0],
                [0, 0, cGamma510/cTau5],
                [cGamma64/cTau6, 0, 0]])

# Define output matrix
MC = np.matrix([[1, 0, 0, 0, 0],
                [0, 1, 0, 0, 0],
                [0, 0, 1, 0, 0],
                [0, 0, 0, 1, 0]])

# Define cost function matrix
MQy = np.matrix(np.diag([0, 0, 1, 0]))

# Define output bounds
ymin = np.matrix([0, 0, 0, 0]).T
ymax = np.matrix([10000, 10000, 12000, 10000]).T

# Define input bounds
umin = np.matrix([0, 0, 0]).T
umax = np.matrix([10000, 500, 500]).T

# Define desired result
yres = np.matrix([0, 0, 10000, 0]).T

# Set up control problem
x = cvx.Variable(5, T+5)
y = cvx.Variable(4, T+5)
u = cvx.Variable(3, T+4)
delta = cvx.Variable(1, T+5)
z = cvx.Variable(1, T+5)
states = []

for t in range(T):
    cost = cvx.quad_form(y[:, t+1]-yres, MQy) + \
            cvx.quad_form(y[:, t+2]-yres, MQy) + \
            cvx.quad_form(y[:, t+3]-yres, MQy)
    constr = [x[:, t+1] == MA*x[:, t]+MB*u[:, t],
                x[:, t+2] == MA*x[:, t+1]+MB*u[:, t+1],
                x[:, t+3] == MA*x[:, t+2]+MB*u[:, t+2],
                y[:, t+1] == MC*x[:, t+1],
                y[:, t+2] == MC*x[:, t+2],
                y[:, t+3] == MC*x[:, t+3],
                # assign a value to delta
                y[2, t+1]-u[0, t] <= delta[:, t+1]*(ymax.item(2)-umin.item(0)),
                y[2, t+2]-u[0, t+1] <= delta[:, t+2]*(ymax.item(2)-umin.item(0)),
                y[2, t+3]-u[0, t+2] <= delta[:, t+3]*(ymax.item(2)-umin.item(0)),
                y[2, t+1]-u[0, t] >= (1-delta[:, t+1])*(ymin.item(2)-umax.item(0)),
                y[2, t+2]-u[0, t+1] >= (1-delta[:, t+2])*(ymin.item(2)-umax.item(0)),
                y[2, t+3]-u[0, t+2] >= (1-delta[:, t+3])*(ymin.item(2)-umax.item(0)),
                # assign 0 to auxiliary variable or bound it
                u[1, t]-z[:, t+1] <= (1-delta[:, t+1])*(umax.item(1)-umin.item(2)),
                u[1, t+1]-z[:, t+2] <= (1-delta[:, t+2])*(umax.item(1)-umin.item(2)),
                u[1, t+2]-z[:, t+3] <= (1-delta[:, t+3])*(umax.item(1)-umin.item(2)),
                u[1, t]-z[:, t+1] >= (1-delta[:, t+1])*(umin.item(1)-umax.item(2)),
                u[1, t+1]-z[:, t+2] >= (1-delta[:, t+2])*(umin.item(1)-umax.item(2)),
                u[1, t+2]-z[:, t+3] >= (1-delta[:, t+3])*(umin.item(1)-umax.item(2)),
                # assign 0 or announced points to auxiliary variable
                z[:, t+1] >= delta[:, t+1]*umin.item(2),
                z[:, t+2] >= delta[:, t+2]*umin.item(2),
                z[:, t+3] >= delta[:, t+3]*umin.item(2),
                z[:, t+1] <= delta[:, t+1]*umax.item(2),
                z[:, t+2] <= delta[:, t+2]*umax.item(2),
                z[:, t+3] <= delta[:, t+3]*umax.item(2),
                # give points if deserved
                u[2, t] == z[:, t],
                u[2, t+1] == z[:, t+1],
                u[2, t+2] == z[:, t+2],
                # bound input
                umin <= u[:, t],
                umin <= u[:, t+1],
                umin <= u[:, t+2],
                u[:, t] <= umax,
                u[:, t+1] <= umax,
                u[:, t+2] <= umax,
                # bound output
                ymin <= y[:, t+1],
                ymin <= y[:, t+2],
                ymin <= y[:, t+3],
                y[:, t+1] <= ymax,
                y[:, t+2] <= ymax,
                y[:, t+3] <= ymax]
    states.append( cvx.Problem(cvx.Minimize(cost), constr) )

prob = sum(states)
prob.solve()

# Plot results
plt.figure(1)
plt.subplot(211)
plt.plot(u[0,:].value.A.flatten())
plt.plot(y[2,:].value.A.flatten())
plt.subplot(212)
plt.plot(delta[0,:].value.A.flatten())
plt.figure(2)
plt.plot(u[1,:].value.A.flatten())
plt.figure(3)
plt.plot(u[2,:].value.A.flatten())
plt.figure(4)
plt.subplot(411)
plt.plot(y[0, :].value.A.flatten())
plt.subplot(412)
plt.plot(y[1, :].value.A.flatten())
plt.subplot(413)
plt.plot(y[2, :].value.A.flatten())
plt.subplot(414)
plt.plot(y[3, :].value.A.flatten())
plt.show()
