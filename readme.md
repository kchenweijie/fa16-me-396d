This repository is for all code generated for Decision and Control of Human-Centered Robotics (ME 396D) in Fall 2016 at the University of Texas at Austin.

If there are any questions, please feel free to contact kevin.chen.weijie@utexas.edu.
